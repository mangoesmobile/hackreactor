mgm.Graph.prototype.updateLinkPaths = function() {
  var self = this;
  this.$links.attr('d', function(d) {
    var sourceX = d.source.x;
    var targetX = d.target.x;
    var sourceY = d.source.y;
    var targetY = d.target.y;
    var deltaX = targetX - sourceX;
    var deltaY = targetY - sourceY;
    var dist = Math.sqrt(deltaX*deltaX + deltaY*deltaY) || 1e-8;
    var normX = deltaX / dist;
    var normY = deltaY / dist;
    var sourcePadding = d.left ? mgm.Graph.nodeRadius*17/12 : mgm.Graph.nodeRadius;
    var targetPadding = d.right ? mgm.Graph.nodeRadius*17/12 : mgm.Graph.nodeRadius;
    var paddedSourceX = sourceX + (sourcePadding * normX);
    var paddedSourceY = sourceY + (sourcePadding * normY);
    var paddedTargetX = targetX - (targetPadding * normX);
    var paddedTargetY = targetY - (targetPadding * normY);
    return 'M' + paddedSourceX + ',' + paddedSourceY + 'L' + paddedTargetX + ',' + paddedTargetY;
  });
};

mgm.Graph.prototype.renderLinks = function() {
  this.$links = this.$links.data(this.linksActivated ? this.links : this.proximityLinks);
  this.$links.call(this.applyMarkerEnds.bind(this));
  this.$links.call(mgm.Util.widenOnMouseOver)
  // for each new link, draw a line and apply event handlers
  this.$links.enter().append('svg:path')
    .attr('class', 'link')
    .call(this.applyMarkerEnds.bind(this))
    .call(mgm.Util.widenOnMouseOver)
    .on('mousedown', this.getSelectedLinkToggler());
  this.$links.exit().remove();
  this.updateLinkPaths();
};

mgm.Graph.prototype.renderBeacons = function() {
  this.$beacons = this.$beacons.data(this.beacons);
  var g = this.$beacons.enter().append('svg:g');
  // for each new beacon draw a new ellipse and apply event handlers
  g.append('svg:ellipse')
    .attr('class', 'beacon')
    .attr('ry', mgm.Graph.beaconRadius)
    .attr('rx', 35)
    .style('fill', 'transparent')
    .style('stroke', 'black')
    .call(mgm.Util.shadeEllipseOnMouseOver)
    .on('mousedown', this.getBeaconClickHandler());
  // for each new beacon, add a label
  g.append('svg:text')
    .attr('class', 'tags')
    .attr('text-anchor', 'middle')
  // fill the label with newline delimited text (SVG's don't easily handle newlines)
  d3.selectAll('text.tags')
    .text(mgm.Util.getBeaconText)
    .each(mgm.Util.insertLineBreaks);
  this.$beacons.exit().remove();
};

mgm.Graph.prototype.renderNodes = function() {
  this.$nodes = this.$nodes.data(this.nodes, function(d){return d.id;});
  this.$nodes.selectAll('circle')
    .style('fill', mgm.Util.getFillColor)
  var g = this.$nodes.enter().append('svg:g');
  // for each new node, draw a new circle and apply event handlers
  g.append('svg:circle')
    .attr('class', 'node')
    .attr('r', mgm.Graph.nodeRadius)
    .style('fill', mgm.Util.getFillColor)
    .style('stroke', function(d) { return d3.rgb(mgm.Util.colors(d.id)).darker().toString(); })
    .call(mgm.Util.enlargeOnMouseOver)
    .on('mousedown', this.getNodeClickHandler())
    .on('mouseup', this.getLinkCreatorUpdater());
  // add a label to the circle
  g.append('svg:text')
    .attr('x', 0).attr('y', 4).attr('class', 'id')
    .text(function(d){
      d._$label = $(this);
      var name = d.get('name');
      return name !== undefined ? name : d.get('id');
    });
  this.$nodes.exit().remove();
};

mgm.Graph.prototype.render = function() {
  this.renderNodes();
  this.renderLinks();
  this.renderBeacons();
  this.force.start();
};

mgm.Graph.prototype.resizeLasso = function(ctx) {
  /* Update the lasso dimensions based on
  where the mouse is, relative to the lasso's origin anchor point.
  */
  var $lasso = this.svg.select('rect.lasso');
  if ($lasso.empty()) return;
  var p = d3.mouse(ctx);
  var lasso = {};
  var deltas = {
    x: p[0] - this.lastMouseDownCoordinates[0],
    y: p[1] - this.lastMouseDownCoordinates[1],
  };
  var direction = mgm.Util.getDirection(deltas).join('');
  if (direction === 'NE') {
    lasso.x = this.lastMouseDownCoordinates[0];
    lasso.y = this.lastMouseDownCoordinates[1] + deltas.y;
    lasso.width = deltas.x;
    lasso.height = Math.abs(deltas.y);
  }
  else if (direction === 'NW') {
    lasso.x = p[0];
    lasso.y = p[1];
    lasso.width = Math.abs(deltas.x);
    lasso.height = Math.abs(deltas.y);
  }
  else if (direction === 'SE') {
    lasso.x = this.lastMouseDownCoordinates[0];
    lasso.y = this.lastMouseDownCoordinates[1];
    lasso.width = deltas.x;
    lasso.height = deltas.y;
  }
  else if (direction === 'SW') {
    lasso.x = this.lastMouseDownCoordinates[0] + deltas.x;
    lasso.y = this.lastMouseDownCoordinates[1];
    lasso.width = Math.abs(deltas.x);
    lasso.height = deltas.y;
  }
  this.lasso = lasso;
  $lasso.attr(lasso);
  this.updateLassoedObjects(lasso);
};

mgm.Graph.prototype.startDraggingArrow = function(fromNode) {
  this.mousedown_node = fromNode;
  var mousedownX = this.mousedown_node.x;
  var mousedownY = this.mousedown_node.y;
  this.drag_line
    .style('marker-end', 'url(#end-arrow)')
    .classed('hidden', false)
    .attr('d', 'M' + mousedownX + ',' + mousedownY + 'L' + mousedownX + ',' + mousedownY);
};

mgm.Graph.prototype.applyMarkerEnds = function(selection) {
  selection
    .style('marker-start', function(d) { return d.left ? 'url(#start-arrow)' : ''; })
    .style('marker-end', function(d) { return d.right ? 'url(#end-arrow)' : ''; });
};

mgm.Graph.prototype.createArrows = function() {
  mgm.Util.createArrow(this.svg, 'start');
  mgm.Util.createArrow(this.svg, 'end');
};
