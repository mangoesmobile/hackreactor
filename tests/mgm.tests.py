import os
import time
import unittest

from AppKit import NSSpeechSynthesizer
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys


voice = NSSpeechSynthesizer.alloc().init()
voice.setVoice_('com.apple.speech.synthesis.voice.Vicki')
def speak(text):
    print text
    voice.startSpeakingString_(text)
    secs = min(4.4, len(text.split(' ')))
    time.sleep(secs)


class MGMGraph(unittest.TestCase):
    results = []

    @classmethod
    def setUpClass(self):
        speak('Welcome to Mangoes Mobile testing suite version zero point 1')
        chromedriver = '/usr/local/Cellar/chromedriver/2.1/bin/chromedriver'
        os.environ['webdriver.chrome.driver'] = chromedriver
        self.driver = webdriver.Chrome(chromedriver)
        self.driver.implicitly_wait(3)
        self.mouse = ActionChains(self.driver)
        self.url = 'http://seriesoftubes-hackreactor.seriesoftubes.nodejitsu.com/'
        self.driver.get(self.url)
        self.grid_view = self.driver.find_element_by_id('gridView')
        self.graph_view = self.driver.find_element_by_id('graphView')
        self.graph = self.driver.find_element_by_id('graph')
        self.select_nodes_button = self.driver.find_elements_by_class_name('mode')[0]
        self.add_node_button = self.driver.find_elements_by_class_name('mode')[1]
        self.go_to_grid_button = self.driver.find_elements_by_class_name('nav')[0]
        self.go_to_graph_button = self.driver.find_elements_by_class_name('nav')[1]

    @classmethod
    def tearDownClass(self):
        if all(MGMGraph.results):
            speak('Congratulations! All tests have passed.')
            self.driver.get('http://viewpure.com/Q3Bp1QVVieM')
            time.sleep(248)
            self.driver.close()
        else:
            failures = sum(1 for result in MGMGraph.results if not result)
            speak('{0} tests failed.'.format(failures))

    def setUp(self):
        self.activate_graph_view()
        time.sleep(1)

    def tearDown(self):
        time.sleep(1)

    def run(self, result=None):
        MGMGraph.results.append(result.wasSuccessful())
        unittest.TestCase.run(self, result)

    def activate_graph_view(self):
        if self.go_to_graph_button.is_displayed():
            self.go_to_graph_button.click()

    def test_nav_buttons(self):
        speak('Testing nav buttons')
        speak('Verifying that clicking on Go to Grid View shows the spreadsheet')
        self.go_to_grid_button.click()
        self.assertTrue(self.grid_view.is_displayed(), 'Grid should be displayed')
        self.assertFalse(self.graph_view.is_displayed(), 'Graph should not be displayed')
        speak('Verified')
        time.sleep(1)
        speak('Verifying that clicking on Go to Graph View shows the graph')
        self.go_to_graph_button.click()
        self.assertFalse(self.grid_view.is_displayed(), 'Grid should not be displayed')
        self.assertTrue(self.graph_view.is_displayed(), 'Graph should be displayed')
        speak('Verified')

    def test_add_node(self):
        speak('Switching mode to add nodes')
        self.add_node_button.click()
        speak('Verifying that clicking on whitespace adds a new node')
        nodes_before = len(self.driver.find_elements_by_css_selector('circle.node'))
        speak('there are currently {0} nodes'.format(nodes_before))
        self.mouse.move_to_element(self.graph).perform()
        self.mouse.move_by_offset(-150, -150).perform()
        speak('clicking')
        self.mouse.click().perform()
        nodes_after = len(self.driver.find_elements_by_css_selector('circle.node'))
        speak('there are now {0} nodes'.format(nodes_after))
        self.assertEquals(nodes_after, nodes_before + 1, 'should have added one node')
        speak('Verified')

    def test_select_nodes(self):
        speak('Verifying that click and drag highlights nodes')
        self.select_nodes_button.click()
        self.mouse.move_to_element(self.graph).perform()
        self.mouse.move_by_offset(-150, -150).perform()
        self.mouse.click_and_hold().perform()
        self.mouse.move_by_offset(250, 250).perform()
        highlighted_nodes = len(self.driver.find_elements_by_css_selector('circle.node.lassoed'))
        speak('{0} nodes are highlighted'.format(highlighted_nodes))
        self.assertGreater(highlighted_nodes, 0, 'should have highlighted at least 1 node')
        speak('Verified')
        speak('Verifying that releasing the mouse brings up {0} node edit views'.format(highlighted_nodes))
        self.mouse.release().perform()
        self.driver.execute_script('window.scrollTo(0, document.body.scrollHeight);')
        node_views = len(self.driver.find_elements_by_css_selector('div.node'))
        speak('{0} node edit views are shown'.format(node_views))
        self.assertEquals(node_views, highlighted_nodes)
        speak('Verified')


if __name__ == '__main__':
    unittest.main()