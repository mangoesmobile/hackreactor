mgm.Graph.prototype.setupLayout = function(data) {
  this.width = 800;
  this.height = 600;

  // this outer/inner SVG layout is necessary for geometric zooming to work
  // outerSVG refers to the group ("svg:g") element on line 14, and contains everything below it
  // including all nodes, beaons, links, etc.
  // So, the outerSVG receives the geometric zoom behavior, which applies to everything below it
  this.outerSVG = d3.select('#graph')
    .append('svg')
      .attr('width', this.width)
      .attr('height', this.height)
      .attr('pointer-events', 'all')
    .append('svg:g');

  this.svg = this.outerSVG.append('svg:g');

  this.svg.append('rect')
    .attr('class', 'viewingArea')
    .attr('width', this.width)
    .attr('height', this.height);

  this.force = d3.layout.force()
    .size([this.width, this.height])
    .nodes(data.nodes)
    .links(data.links)
    .linkDistance(188) // adjust this to set min distance for linked nodes
    .linkStrength(0.88) // when this is lower, it means links are very flexible
    .charge(0)
    .gravity(0);

  this.createArrows();

  this.drag_line = this.svg.append('svg:path')
    .attr('class', 'link dragline hidden')
    .attr('d', 'M0,0L0,0');

  // don't show this button until later (conditional on nodes selected)
  $('#intertwineButton').hide();

  this.zoomer = d3.behavior.zoom()
    .on('zoom', this.handleZoom.bind(this))
    .scaleExtent([1, 8]);
  this.zoomScale = 1;
};

mgm.Graph.prototype.resolveCollisions = function(alpha) {
  /* Push nodes apart if they are overlapping */
  var padding = 1;
  //TODO: store the quadtree and update it whenever nodes added/removed
  //then this becomes = this.quadtree
  var quadtree = d3.geom.quadtree(this.nodes);
  return function(d) {
    var r = mgm.Graph.nodeRadius + 1 + padding;
    var nx1 = d.x - r;
    var nx2 = d.x + r;
    var ny1 = d.y - r;
    var ny2 = d.y + r;
    quadtree.visit(function(quad, x1, y1, x2, y2) {
      if (quad.point && (quad.point !== d)) {
        var x = d.x - quad.point.x;
        var y = d.y - quad.point.y;
        var dist = Math.sqrt(x*x + y*y);
        var r = mgm.Graph.nodeRadius*2 + padding;
        if (dist < r) {
          dist = (dist - r)/dist * alpha;
          if (!(d instanceof mgm.Beacon)) {
            d.x -= x *= dist;
            d.y -= y *= dist;
          }
          if (!(quad.point instanceof mgm.Beacon)) {
            quad.point.x += x;
            quad.point.y += y;
          }
        }
      }
      return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
    });
  };
};

mgm.Graph.prototype.getBestLocationByNodeId = function(nodes, beacons) {
  /* Based on how each node matches each beacon,
  calculate the ideal location of each node
  */
  var bestLocationByNodeId = {};
  nodes.forEach(function(node) {
    var matchingBeacons = [];
    beacons.forEach(function(beacon) {
      if (beacon.matches(node)) {
        matchingBeacons.push(beacon);
      }
    });
    if (matchingBeacons.length === 0) {
      // use a default non-matched location
      bestLocationByNodeId[node.id] = {x: 0, y: 0};
    }
    else if (matchingBeacons.length === 1) {
      bestLocationByNodeId[node.id] = matchingBeacons[0];
    }
    else if (matchingBeacons.length === 2) {
      bestLocationByNodeId[node.id] = {
        x: (matchingBeacons[0].x + matchingBeacons[1].x) / 2,
        y: (matchingBeacons[0].y + matchingBeacons[1].y) / 2
      };
    }
    else {
      bestLocationByNodeId[node.id] = mgm.Util.getCentroid(matchingBeacons);
    }
    // if the future bestlocation of a node is out in the middle of nowhere,
    // let the app know, because it will change how the node is arranged around the location
    matchingBeacons.length > 1 && (bestLocationByNodeId[node.id].isEstimate = true);
  });
  return bestLocationByNodeId;
};

mgm.Graph.prototype.moveTowardsBeacons = function (nodes, beacons, alpha) {
  var locationByNodeId = this.getBestLocationByNodeId(nodes, beacons);
  return function(node) {
    var location = locationByNodeId[node.id];
    if (location.isEstimate) {
      // there is no beacon to surround-- it's in the middle
      // so make the node converge right on top of the location
      var dx = node.x - location.x;
      var dy = node.y - location.y;
      var dist = Math.sqrt(dx*dx + dy*dy);
      if (dist !== 0) {
        node.x -= dx * alpha;
        node.y -= dy * alpha;
      }
    }
    else {
      // there is an actual beacon, so try to make the nodes gather around it in a ring
      var dx = node.x;
      dx -= node.x > location.x ? location.x + mgm.Graph.beaconRadius : location.x - mgm.Graph.beaconRadius;
      var dy = node.y;
      dy -= node.y > location.y ? location.y + mgm.Graph.beaconRadius : location.y - mgm.Graph.beaconRadius;
      var dist = Math.sqrt(dx*dx + dy*dy);
      var r = mgm.Graph.nodeRadius + mgm.Graph.beaconRadius;
      if (dist !== r) {
        node.x -= dx * alpha;
        node.y -= dy * alpha;
      }
    }
  };
};

mgm.Graph.prototype.keepNodesInBounds = function(){
  var r = mgm.Graph.nodeRadius;
  var self = this;
  this.nodes.forEach(function(d){
    d.x = Math.max(r, Math.min(self.width - r, d.x));
    d.y = Math.max(r, Math.min(self.height - r, d.y));
  });
};

mgm.Graph.prototype.reposition = function(d) {
  return 'translate(' + d.x + ',' + d.y + ')';
};

mgm.Graph.prototype.repositionThings = function() {
  this.$nodes.attr('transform', this.reposition.bind(this));
  this.$beacons.attr('transform', this.reposition.bind(this));
  this.updateLinkPaths();
};

mgm.Graph.prototype.fastForward = function(alpha, max) {
  /* Skip past the usual slow, gradual re-rendering of the graph
  to save CPU resources
  */
  alpha = alpha || 0;
  max = max || 1000;
  var i = 0;
  while(this.force.alpha() > alpha && i++ < max) this.force.tick();
  this.repositionThings();
  this.force.stop();
};

mgm.Graph.prototype.removeLasso = function() {
  this.svg.selectAll('rect.lasso').remove();
  this.lasso = {};
  this.trigger('changedLassoedNodes');
};

mgm.Graph.prototype.removeLassoedNodes = function() {
  d3.selectAll('.lassoed').classed('lassoed', false);
  this.lassoedNodes = [];
  this.trigger('changedLassoedNodes');
};

mgm.Graph.prototype.removeSelectedBeacons = function() {
  d3.selectAll('.beacon.selected').classed('selected', false);
  this.selectedBeacons = [];
  this.trigger('changedSelectedBeacons');
};

mgm.Graph.prototype.removeSelectedLinks = function() {
  d3.selectAll('.link.selected').classed('selected', false);
  this.selectedLinks = [];
};
