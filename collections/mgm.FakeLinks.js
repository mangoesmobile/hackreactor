mgm.FakeLinks = function() {
  /* Special data structure to contain edges
  that are not quite ready to be displayed in the graph,
  but should be displayed in the grid
  */
  mgm.Links.apply(this, arguments);
};

mgm.FakeLinks.prototype = Object.create(mgm.Links.prototype);
mgm.FakeLinks.constructor = mgm.FakeLinks;

mgm.FakeLinks.prototype.fetch = function(){};

mgm.FakeLinks.prototype.create = function() {
  var link = mgm.Links.prototype.create.apply(this, arguments);
  link._fake = true;
  return link;
};

mgm.FakeLinks.prototype.remove = function() {
  var link = mgm.Links.prototype.remove.apply(this, arguments);
  delete link._fake;
  return link;
};
