mgm.Links = function(nodes) {
  MicroEvent.mixin(this);
  this.nodes = nodes;
  this.lastId = -1;
  this.fetch();
};

mgm.Links.prototype = Object.create(Array.prototype);
mgm.Links.constructor = mgm.Links;


mgm.Links.prototype.create = function(data, options) {
  var link = new mgm.Link({
    id: ++this.lastId,
    source: this.nodes.get(data.sourceNodeID),
    sourceNodeID: data.sourceNodeID,
    target: this.nodes.get(data.targetNodeID),
    targetNodeID: data.targetNodeID,
    left: data.left,
    right: data.right
  });
  this.push(link);
  if (options.updateGraph) {
    link.added = true;
    this.trigger('added', link);
  }
  if (options.updateGrid) {
    this.trigger('addToGrid', link);
  }
  return link;
};

mgm.Links.prototype.fetch = function() {
  // TODO: once a link from A to B has been created
  // don't create link from B to A or A to B again
  var createOptions = {updateGraph: false, updateGrid: true};
  var n = this.nodes.length;
  for (var i = 0; i < mgm.Util.randBetween(1, n); i++) {
    var left = Math.random() < 0.5 ? true : false;
    var right = !left || Math.random() < 0.5 ? true : false
    var sourceNodeID = mgm.Util.randBetween(0, n-1);
    var targetNodeID = mgm.Util.randBetween(0, n-1);
    if (sourceNodeID !== targetNodeID) {
      this.create({
        sourceNodeID: sourceNodeID,
        targetNodeID: targetNodeID,
        left: left,
        right: right
      }, createOptions);
    }
  }
  this.trigger('added');
};

mgm.Links.prototype.remove = function(link, options) {
  var removed = this.splice(this.indexOf(link), 1)[0];
  if (options.updateGraph) this.trigger('removed', removed);
  if (options.updateGrid) this.trigger('removeFromGrid', removed);
  return removed;
};

mgm.Links.prototype.get = function(id) {
  // TODO: use index or binary search
  for (var i = 0; i < this.length; i++) {
    if (this[i].id === id) return this[i];
  }
};

mgm.Links.prototype.update = function(id, data) {
  var link = this.get(id);
  for (var key in data) {
    link[key] = data[key];
  }
  // if the link has a node id, but no matching node
  // or has an id, but the node doesn't match up to the id (due to update)
  // update the link's node(s)
  if (link.sourceNodeID !== undefined &&
    (!link.source || link.sourceNodeID !== link.source.id)) {
    link.source = this.nodes.get(link.sourceNodeID);
  }
  if (link.targetNodeID !== undefined &&
    (!link.target || link.targetNodeID !== link.target.id)) {
    link.target = this.nodes.get(link.targetNodeID);
  }
  // IFF the link is 100% complete, can we add it to the graph
  // if this isn't the case, the graph will throw errors due to missing attributes
  if (!link.added && link.isValid()) {
    link.added = true;
    this.trigger('addToGraph', link);
  }
  // IFF the link is still 100% complete after a change, will we trigger a change
  else if (link.added && link.isValid()) {
    this.trigger('changed');
  }
  return link;
};
