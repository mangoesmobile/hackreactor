mgm.Nodes = function() {
  MicroEvent.mixin(this);
  this.lastId = -1;
  this.fetch();
};

mgm.Nodes.prototype = Object.create(Array.prototype);
mgm.Nodes.prototype.constructor = mgm.Nodes;

mgm.Nodes.prototype.onChanged = function(node) {
  this.trigger('changed', node);
};

mgm.Nodes.prototype.create = function(data, silent) {
  data.id = (++this.lastId);
  var node = new mgm.Node(data);
  node.bind('changed', this.onChanged.bind(this));
  this.push(node);
  !silent && this.trigger('added', node);
  return node;
};

mgm.Nodes.prototype.get = function(id) {
  // TODO: use index or binary search
  for (var i = 0; i < this.length; i++) {
    if (this[i].id === id) return this[i];
  }
};

mgm.Nodes.prototype.fetch = function() {
  var n = 15;
  for (var i = 0; i < n; i++) {
    this.create({
      x: mgm.Util.randBetween(1, 400),
      y: mgm.Util.randBetween(1, 300),
      fixed: false,
      attributes: {name: i}
    });
  }
};

mgm.Nodes.prototype.remove = function(node, silent) {
  var removed = this.splice(this.indexOf(node), 1)[0];
  !silent && this.trigger('removed', removed);
  return removed;
};

mgm.Nodes.prototype.update = function(node) {
  // TODO: bind this to node.on('save')
};
