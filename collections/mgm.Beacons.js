mgm.Beacons = function() {
  MicroEvent.mixin(this);
  this.fetch();
};

mgm.Beacons.prototype = Object.create(Array.prototype);
mgm.Beacons.constructor = mgm.Beacons;

mgm.Beacons.prototype.onChanged = function(beacon) {
  this.trigger('changed', beacon);
};

mgm.Beacons.prototype.create = function(data) {
  var beacon = new mgm.Beacon(data);
  beacon.bind('changed', this.onChanged.bind(this));
  this.push(beacon);
  this.trigger('added', beacon);
  return beacon;
};

mgm.Beacons.prototype.remove = function(beacon, silent) {
  var removed = this.splice(this.indexOf(beacon), 1)[0];
  !silent && this.trigger('removed', removed);
  return removed;
};

mgm.Beacons.prototype.fetch = function() {
  this.create({x: 400, y: 300, fixed: true});
};
