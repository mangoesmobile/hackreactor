mgm.Util = {};

mgm.Util.shadeEllipseOnMouseOver = function(selection) {
  selection.on('mouseover', function(){
    var shape = d3.select(this);
    shape.style('fill', 'DeepSkyBlue')
  }).on('mouseout', function(){
    var shape = d3.select(this);
    shape.style('fill', 'none')
  });
};

mgm.Util.enlargeOnMouseOver = function(selection) {
  selection.on('mouseover', function(d) {
    d3.select(this).attr('transform', 'scale(1.2)');
  })
  .on('mouseout', function(d) {
    d3.select(this).attr('transform', '');
  });
};

mgm.Util.widenOnMouseOver = function(selection) {
  selection.on('mouseover', function(){
    d3.select(this).style('stroke-width', '6px');
  }).on('mouseout', function(){
    d3.select(this).style('stroke-width', '4px');
  });
};

mgm.Util.getOpposite = function(direction) {
  return direction === 'left' ? 'right' : 'left';
};

mgm.Util.colors =  d3.scale.category10();
mgm.Util.getFillColor = function(node){ return mgm.Util.colors(node.id); };

mgm.Util.createArrow = function(svg, type) {
  svg.append('svg:defs').append('svg:marker')
      .attr('id', type + '-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', type === 'start' ? 4 : 6)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
    .append('svg:path')
      .attr('d', type === 'start' ? 'M10,-5L0,0L10,5' : 'M0,-5L10,0L0,5')
      .attr('fill', '#000');
};

mgm.Util.insertLineBreaks = function(d) {
  var el = d3.select(this);
  var words = this.textContent.split('\n');
  el.text('');
  for (var i = 0; i < words.length; i++) {
    var tspan = el.append('tspan').text(words[i]);
    if (i > 0) tspan.attr('x', 0).attr('dy', '15');
  }
};

mgm.Util.getBeaconText = function(beacon) {
  var lines = [];
  for (var key in beacon.attributes) {
    if (beacon.attributes[key] !== undefined) {
      lines.push(key + ': ' + beacon.attributes[key]);
    }
  }
  !lines.length && lines.push('*');
  return lines.join('\n');
};

mgm.Util.randBetween = function(lo, hi){return Math.floor(Math.random()*hi) + lo};

mgm.Util.defaults = function(obj) {
  $.each(Array.prototype.slice.call(arguments, 1), function(idx, source) {
    if (source) {
      for (var prop in source) {
        if (obj[prop] === void 0) obj[prop] = source[prop];
      }
    }
  });
  return obj;
};

mgm.Util.renameProperty = function(obj, oldKey, newKey) {
  if (oldKey in obj) {
    Object.defineProperty(obj, newKey,
      Object.getOwnPropertyDescriptor(obj, oldKey));
    delete obj[oldKey];
  }
};

mgm.Util.getDirection = function(mouseDeltas) {
  // get direction like NE NW SE SW
  var direction = [];
  if (mouseDeltas.x > 0) { //right
    direction[1] = 'E';
  }
  else if (mouseDeltas.x < 0) {
    direction[1] = 'W';
  }
  if (mouseDeltas.y > 0) { //down
    direction[0] = 'S';
  }
  else if (mouseDeltas.y < 0) {
    direction[0] = 'N';
  }
  return direction;
};

mgm.Util.processCombinationsOf2 = function(things, cb) {
  for (var i = 0; i < things.length; i++) {
    for (var j = i + 1; j < things.length; j++) {
      cb(things[i], things[j]);
    }
  }
};

//Below this point, all functions are helpers for determining the centroid
mgm.Util.Point = function(x,y) {
  this.x = x;
  this.y = y;
};

mgm.Util.Point.prototype.distance = function(that) {
  var dX = that.x - this.x;
  var dY = that.y - this.y;
  return Math.sqrt(dX*dX + dY*dY);
};

mgm.Util.Point.prototype.slope = function(that) {
  var dX = that.x - this.x;
  var dY = that.y - this.y;
  return dY / dX;
};

mgm.Util.Contour = function(points) {
  this.upper = this.getUpperLeft(points);
  // for centroid to work, points must be sorted in clockwise order
  points.sort(this.sortPointsClockwise.bind(this));
  this.pts = points;
};

mgm.Util.Contour.prototype.getUpperLeft = function(points) {
  // Find the upper most point. In case of a tie, get the left most point.
  var top = points[0];
  for (var i = 1; i < points.length; i++) {
    var temp = points[i];
    if (temp.y > top.y || (temp.y == top.y && temp.x < top.x)) {
      top = temp;
    }
  }
  return top;
};

mgm.Util.Contour.prototype.sortPointsClockwise = function(p1, p2) {
  // A custom sort function that sorts p1 and p2 based on their slope
  // that is formed from the upper most point from the array of points.

  // Exclude the 'upper' point from the sort (which should come first).
  if (p1 === this.upper) return -1;
  if (p2 === this.upper) return 1;

  // Find the slopes of 'p1' and 'p2' when a line is 
  // drawn from those points through the 'upper' point.
  var m1 = this.upper.slope(p1);
  var m2 = this.upper.slope(p2);

  // 'p1' and 'p2' are on the same line towards 'upper'.
  if (m1 === m2) {
    // The point closest to 'upper' will come first.
    return p1.distance(this.upper) < p2.distance(this.upper) ? -1 : 1;
  }

  // If 'p1' is to the right of 'upper' and 'p2' is the the left.
  if(m1 <= 0 && m2 > 0) return -1;

  // If 'p1' is to the left of 'upper' and 'p2' is the the right.
  if(m1 > 0 && m2 <= 0) return 1;

  // It seems that both slopes are either positive, or negative.
  return m1 > m2 ? -1 : 1;
};

mgm.Util.Contour.prototype.area = function() {
  var area = 0;
  var pts = this.pts;
  var nPts = pts.length;
  var j = nPts - 1;
  var p1, p2;
  for (var i = 0; i < nPts; j = i++) {
    p1 = pts[i];
    p2 = pts[j];
    area += p1.x * p2.y;
    area -= p1.y * p2.x;
  }
  return area / 2;
};

mgm.Util.Contour.prototype.getCentroid = function() {
   var pts = this.pts;
   var nPts = pts.length;
   var x = 0;
   var y = 0;
   var j = nPts - 1;
   var f, p1, p2;
   for (var i = 0; i < nPts; j = i++) {
      p1 = pts[i];
      p2 = pts[j];
      f = p1.x*p2.y - p2.x*p1.y;
      x += (p1.x + p2.x) * f;
      y += (p1.y + p2.y) * f;
   }
   f = this.area() * 6;
   return new mgm.Util.Point(x/f, y/f);
};

mgm.Util.getCentroid = function(xyPoints) {
  var points = [];
  for (var i = 0; i < xyPoints.length; i++) {
    //y can never be negative
    var y = xyPoints[i].y;
    if (y < 0) throw 'All points\' y-values must be non-negative.'
    points.push(new mgm.Util.Point(xyPoints[i].x, y));
  }
  var contour = new mgm.Util.Contour(points);
  return contour.getCentroid();
};
