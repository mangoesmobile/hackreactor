mgm.App = function() {
  this.$graphView = $('#graphView'); //has the graph
  this.$gridView = $('#gridView'); //the whole grid area (which can contain either link or node grid)  
  this.view = null;

  var data = this.getData();
  // TODO: move the editview to within the graph
  this.graph = new mgm.Graph(data);
  this.editView = new mgm.EditView(this.graph);
  this.grid = new mgm.Grid(data);

  this.setView('graph');

  this.addEvents();
};

mgm.App.prototype.setView = function(newView) {
  if (this.view === newView) return;
  if (newView === 'graph') {
    this.$gridView.hide();
    this.setKeyboard('graph');
    this.$graphView.show();
  }
  else if (newView === 'grid') {
    this.$graphView.hide();
    this.setKeyboard('grid');
    this.$gridView.show();
  }
  this.view = newView;
};

mgm.App.prototype.setKeyboard = function(view) {
  this[view].setupKeyboard();
};

mgm.App.prototype.handleNavButtonClick = function() {
  // assumes there are only 2 views: grid and graph,
  // and a click on the nav button means flip between them
  if (this.view === 'graph') {
    this.setView('grid');
  }
  else {
    this.setView('graph');
  }
};

mgm.App.prototype.addEvents = function() {
  $(document).on('click', 'button.nav',    this.handleNavButtonClick.bind(this));

  var renderEditView = this.editView.render.bind(this.editView);
  // When the graph changes its lassoed nodes, or the nodes themselves change,
  // update the edit view below the graph.
  // and if there are 2+ nodes selected, display the intertwine button
  // TODO: move these to graph (including the edit view)
  // TODO: make a graphView
  this.graph.bind('changedLassoedNodes',    renderEditView);
  this.graph.nodes.bind('changed',          renderEditView);
  // flip to a beacon edit view if the beacon selection changes or the beacons themselves change
  this.graph.bind('changedSelectedBeacons', renderEditView);
  this.graph.beacons.bind('changed',        renderEditView);

  this.graph.bind('beginGridTransaction',   this.grid.beginTransaction.bind(this.grid));
  this.graph.bind('endGridTransaction',     this.grid.endTransaction.bind(this.grid));

  // When messing with attributes, the order in which these events fire, matters.
  // First, the edit views under the graph must update, then the wizardview under the grid.
  var renderNodeWizardView = this.grid.nodeGridView.wizardView.render.bind(this.grid.nodeGridView.wizardView);
  this.grid.nodeGridView.bind('addedAttribute',   renderEditView);
  this.grid.nodeGridView.bind('addedAttribute',   renderNodeWizardView);
  this.grid.nodeGridView.bind('removedAttribute', renderEditView);
  this.grid.nodeGridView.bind('removedAttribute', renderNodeWizardView);
  this.grid.nodeGridView.bind('renamedAttribute', renderEditView);
  this.grid.nodeGridView.bind('renamedAttribute', renderNodeWizardView);
  var renderLinkWizardView = this.grid.linkGridView.wizardView.render.bind(this.grid.linkGridView.wizardView);
  this.grid.linkGridView.bind('addedAttribute',   renderEditView);
  this.grid.linkGridView.bind('addedAttribute',   renderLinkWizardView);
  this.grid.linkGridView.bind('removedAttribute', renderEditView);
  this.grid.linkGridView.bind('removedAttribute', renderLinkWizardView);
  this.grid.linkGridView.bind('renamedAttribute', renderEditView);
  this.grid.linkGridView.bind('renamedAttribute', renderLinkWizardView);
};

mgm.App.prototype.getData = function() {
  var nodes = new mgm.Nodes();
  var links = new mgm.Links(nodes);
  var beacons = new mgm.Beacons();
  return {nodes: nodes, links: links, beacons: beacons};
};
