mgm.Graph.prototype.getMouseUpHandler = function() {
  var self = this;
  return function() {
    // if a node was held down and dragged from, an arrow was created
    // now that the mouse is up, remove the arrow
    if (self.mousedown_node) {
      self.drag_line
        .classed('hidden', true)
        .style('marker-end', '');
    }
    else {
      self.removeLasso();
    }
    if (self.beaconMoved) {
      self.render();
      self.beaconMoved = false;
    }
    self.resetMouseVars();
  };
};

mgm.Graph.prototype.getKeyUpHandler = function() {
  var self = this;
  return function() {
    self.lastKeyDown = -1;
  };
};

mgm.Graph.prototype.getLassoCreator = function() {
  var self = this;
  return function() {
    if (self.mousedown_node || self.mousedown_link || self.mousedown_beacon) return;
    // clear the selections (if any) from the previous lasso
    self.removeSelectedLinks();
    self.removeLassoedNodes();
    self.removeSelectedBeacons();
    var point = d3.mouse(this);
    self.lastMouseDownCoordinates = point; //anchor the new lasso here
    self.svg.append('rect').attr({
      class: 'lasso',
      rx: 6, ry: 6,
      width: 0, height: 0,
      x: point[0], y: point[1]
    });
  };
};

mgm.Graph.prototype.getKeyDownHandler = function() {
  var self = this;
  var keyCodeHandlers = {
    8: this.handleDelete, //DELETE
    46: this.handleDelete, //DELETE
    32: this.getLinkTypeChanger() //SPACEBAR
  };
  return function() {
    if (!(d3.event.keyCode in keyCodeHandlers)) return;
    d3.event.preventDefault();
    if (self.lastKeyDown !== -1) return;
    self.lastKeyDown = d3.event.keyCode;
    var fn = keyCodeHandlers[d3.event.keyCode];
    fn.call(self);
  };
};

mgm.Graph.prototype.moveBeacon = function() {
  /* When dragging a beacon, update its position within the bounds of the svg */
  var yBuffer = mgm.Graph.beaconRadius;
  var xBuffer = mgm.Graph.beaconXRadius;
  var p = d3.svg.mouse(this.svg[0][0]);
  this.mousedown_beacon.y = (
    Math.max(
      yBuffer, Math.min(this.height - yBuffer, p[1])
    )
  );
  this.mousedown_beacon.x = (
    Math.max(
      xBuffer, Math.min(this.width - xBuffer, p[0])
    )
  );
  this.force.alpha() && this.force.stop();
  this.beaconMoved = true;
  this.repositionThings();
};

mgm.Graph.prototype.moveDragLine = function(ctx) {
  /* When dragging from a node, start updating the arrow line */
  var mousedownX = this.mousedown_node.x;
  var mousedownY = this.mousedown_node.y;
  var moveTo = 'M' + mousedownX + ',' + mousedownY;
  var lineTo = 'L' + d3.mouse(ctx)[0] + ',' + d3.mouse(ctx)[1];
  this.drag_line.attr('d', moveTo + lineTo);
};

mgm.Graph.prototype.getMouseMoveHandler = function() {
  var self = this;
  return function() {
    if (self.mousedown_node) {
      self.moveDragLine(this);
    }
    else if (self.mousedown_beacon) {
      self.moveBeacon();
    }
    else if (!self.mousedown_link) {
      self.resizeLasso(this);
    }
  };
};

mgm.Graph.prototype.getNodeAdder = function() {
  var self = this;
  return function() {
    if (self.mousedown_node || self.mousedown_link || self.mousedown_beacon) return;
    var point = d3.mouse(this);
    self.nodes.create({
      x: point[0],
      y: point[1],
      fixed: false
    });
  };
};

mgm.Graph.prototype.getBeaconAdder = function() {
  var self = this;
  return function() {
    if (self.mousedown_node || self.mousedown_link || self.mousedown_beacon) return;
    var point = d3.mouse(this);
    self.beacons.create({
      x: point[0],
      y: point[1],
      fixed: true
    });
  };
};

mgm.Graph.prototype.getLinkCreatorUpdater = function() {
  var self = this;
  return function(mouseup_node) {
    if (!self.mousedown_node) return;
    self.drag_line.classed('hidden', true).style('marker-end', '');
    if (mouseup_node === self.mousedown_node) {
      // don't allow a link to be made from a node to the same node
      self.resetMouseVars();
      return;
    }
    var options = {updateGraph: true, updateGrid: true};
    self.createOrUpdateLink(self.mousedown_node, mouseup_node, options);
  };
};

mgm.Graph.prototype.getNodeClickHandler = function() {
  var self = this;
  return function(node) {
    // don't allow user to edit nodes and beacons/links at once
    self.removeSelectedBeacons();
    self.removeSelectedLinks();

    if (this.classList.contains('lassoed')) {
      this.classList.remove('lassoed');
      self.lassoedNodes.splice(self.lassoedNodes.indexOf(node), 1);
    }
    else {
      this.classList.add('lassoed');
      self.lassoedNodes.push(node);
    }
    self.trigger('changedLassoedNodes');
    self.startDraggingArrow(node);
  };
};

mgm.Graph.prototype.getBeaconClickHandler = function() {
  var self = this;
  return function(beacon) {
    // don't allow user to edit beacons and nodes/links at once
    self.removeLassoedNodes();
    self.removeSelectedLinks();

    self.resetMouseVars();
    self.mousedown_beacon = beacon;
    if (this.classList.contains('selected')) {
      this.classList.remove('selected');
      self.selectedBeacons.splice(self.selectedBeacons.indexOf(beacon), 1);
    }
    else {
      this.classList.add('selected');
      self.selectedBeacons.push(beacon);
    }
    self.trigger('changedSelectedBeacons');
  };
};

mgm.Graph.prototype.getSelectedLinkToggler = function() {
  var self = this;
  return function(link) {
    // don't allow user to edit links and beacons/nodes at once
    self.removeSelectedBeacons();
    self.removeLassoedNodes();

    self.mousedown_link = link;
    if (this.classList.contains('selected')) {
      this.classList.remove('selected');
      self.selectedLinks.splice(self.selectedLinks.indexOf(link), 1);
    }
    else {
      this.classList.add('selected');
      self.selectedLinks.push(link);
    }
  };
};

mgm.Graph.prototype.handleDelete = function() {
  this.deleteLassoedNodes();
  this.deleteSelectedLinks();
  this.deleteSelectedBeacons();
};

mgm.Graph.prototype.getLinkTypeChanger = function() {
  /* Cycle through link directions after hitting SPACEBAR */
  var direction = 'right';
  var nextDirection = {
    'both': 'left',
    'left': 'right',
    'right': 'both'
  };
  var self = this;
  return function() {
    direction = nextDirection[direction];
    for (var i = 0; i < self.selectedLinks.length; i++) {
      var link = self.selectedLinks[i];
      if (direction === 'both') {
        link.left = true;
        link.right = true;
      }
      else {
        link[direction] = true;
        link[mgm.Util.getOpposite(direction)] = false;
      }
    }
    self.links.trigger('changed');
    self.links.trigger('updateGrid');
  };
};

mgm.Graph.prototype.handleIntertwineClick = function(e) {
  this.trigger('beginGridUpdate', 'links');
  // need to update the grid for each new link, but not the graph
  // remove any links that are attached to lassoed nodes
  var removeOptions = {updateGraph: false, updateGrid: true};
  var self = this;
  this.links.forEach(function(link){
    // TODO: store selected state in each node and link and beacon
    if (self.lassoedNodes.indexOf(link.source) >= 0 &&
        self.lassoedNodes.indexOf(link.target) >= 0
      ) {
      self.links.remove(link, removeOptions);
    }
  });
  // now we can create links without checking to see if they exist first
  var createOptions = {updateGraph: false, updateGrid: true};
  mgm.Util.processCombinationsOf2(this.lassoedNodes,
  function(node1, node2){
    self.createOrUpdateLink(node1, node2, createOptions);
  });
  this.trigger('endGridUpdate', 'links');
  this.links.trigger('addToGraph');
  $('#progress').show();
  $('#progress').delay(500).fadeOut(400);
};

mgm.Graph.prototype.handleZoom = function() {
  /* Repositions things on the screen to do geometric zooming */
  this.zoomScale = d3.event.scale;
  this.svg.attr('transform',
    'translate(' + d3.event.translate
    + ')scale(' + d3.event.scale + ')'
  );
};

mgm.Graph.prototype.addZoomBehavior = function() {
  var self = this;
  this.outerSVG
    .call(self.zoomer.scale(self.zoomScale));
};

mgm.Graph.prototype.removeZoomBehavior = function() {
  this.outerSVG
    .call(d3.behavior.zoom().on('zoom', null));
};

mgm.Graph.prototype.changeMode = function(newMode) {
  if (this.mode === newMode) return;
  this.mode = newMode;
  var mousedownHandlers = {
    'selectNodes': this.getLassoCreator(),
    'selectLinks': this.getLassoCreator(),
    'addNodes': this.getNodeAdder(),
    'addBeacons': this.getBeaconAdder()
  };
  this.svg.on('mousedown', mousedownHandlers[this.mode]);
  this.toSelect = newMode === 'selectLinks' ? 'links' : 'nodes';

  if (this.mode === 'zoom') {
    this.addZoomBehavior();
  }
  else {
    // must remove zoom behavior, otherwise dragging across the screen will pan the view
    // instead of drawing the lasso
    this.removeZoomBehavior();
  }
};

mgm.Graph.prototype.handleModeButtonClick = function(evt) {
  var btn = $(evt.target);
  btn.siblings().each(function(idx, sibling){
    var $sibling = $(sibling);
    $sibling.hasClass('mode') && $sibling.removeClass('activated');
  });
  btn.addClass('activated');
  this.changeMode(btn.data('mode'));
};

mgm.Graph.prototype.possiblyDisplayIntertwineButton = function() {
  if (this.lassoedNodes.length > 1) {
    $('#intertwineButton').show();
  }
  else {
    $('#intertwineButton').hide();
  }
};

mgm.Graph.prototype.possiblyRewireKeyBoard = function() {
  /* Re-bind keyup/down to graph events only if *none* of document's .field inputs are focused */
  var noInputBoxesFocused = true;
  $('.field').each(function(idx, field){
    if (noInputBoxesFocused) {
      $(field).is(':focus') && (noInputBoxesFocused = false);
    }
  });
  if (noInputBoxesFocused) this.addKeyHandlers();
  else this.removeKeyHandlers();
};
