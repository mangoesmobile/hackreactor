mgm.Link = function(data) {
  MicroEvent.mixin(this);
  for (var key in data) {
    this[key] = data[key];
  }
  // this weird structure is only used because the .attributes property
  // is expected by the spreadsheet and by the edit views
  this.attributes = this;
};

mgm.Link.metadataByVariableName = {
  'sourceNodeID': {'fieldType': 'integer'},
  'targetNodeID': {'fieldType': 'integer'},
  'left': {'fieldType': 'boolean'},
  'right': {'fieldType': 'boolean'}
};

mgm.Link.prototype.update = function(data) {
  var somethingChanged = false;
  for (var key in data) {
    if (key !== 'id' && data[key] !== undefined) {
      this[key] = data[key];
      somethingChanged = true;
    }
  }
  if (!silent && somethingChanged) this.trigger('changed', this);
};

mgm.Link.prototype.isValid = function() {
  return (
    this.left !== undefined &&
    this.right !== undefined &&
    this.source !== undefined &&
    this.target !== undefined
  );
};
