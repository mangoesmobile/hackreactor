mgm.Beacon = function(data) {
  mgm.Node.apply(this, arguments);
};

mgm.Beacon.prototype = Object.create(mgm.Node.prototype);
mgm.Beacon.constructor = mgm.Beacon;

mgm.Beacon.prototype.matches = function(node) {
  // for each field in this beacon that isn't undefined,
  // see if there's an exact match
  // TODO: allow for fuzzy matching using regexes in this beacon's fields
  for (var key in this.attributes) {
    var val = this.attributes[key];
    if (val !== undefined) {
      if (val !== node.attributes[key]) return false;
    }
  }
  return true;
};
