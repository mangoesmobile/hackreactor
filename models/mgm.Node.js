mgm.Node = function(data) {
  MicroEvent.mixin(this);
  for (key in data) this[key] = data[key];
  this.x = this.x || mgm.Util.randBetween(10, 400);
  this.y = this.y || mgm.Util.randBetween(10, 400);
  this.fixed = false;
  var defaultAttributes = this.getDefaultAttributes();
  // what if .attributes is missing entirely?
  this.attributes = this.attributes || defaultAttributes;
  this.attributes.id = this.id;
  // what if .attributes is only {name: 'Bob'}? must give defaults
  mgm.Util.defaults(this.attributes, defaultAttributes);
  this.updateSVGAppearance();
};

// default attributes (in the future, these should probably be taken from a server)
mgm.Node.metadataByVariableName = {
  id: {fieldType: 'integer', readOnly: true},
  name: {fieldType: 'string'},
  latitude: {fieldType: 'float'},
  longitude: {fieldType: 'float'},
  numberTalkingTo: {fieldType: 'integer'},
  commDirection: {fieldType: 'choice', choices:['', 'Both', 'From me', 'To me']},
  realTimeComm: {fieldType: 'boolean'},
  usesAudio: {fieldType: 'boolean'},
  usesText: {fieldType: 'boolean'},
  usesVideo: {fieldType: 'boolean'},
  locationTagged: {fieldType: 'boolean'}
};


mgm.Node.prototype.getDefaultAttributes = function() {
  var defaultAttributes = {};
  for (var variableName in mgm.Node.metadataByVariableName) {
    defaultAttributes[variableName] = undefined;
  }
  return defaultAttributes;
};

mgm.Node.prototype.get = function(name) {
  return this.attributes[name];
};

mgm.Node.prototype.updateSVGAppearance = function() {
  if (this._$label) {
    var name = this.get('name');
    var label = name !== undefined ? name : this.get('id');
    this._$label.text(label);
  }
};

mgm.Node.prototype.set = function(name, value, silent) {
  // always interpret an emptystring value as undefined
  // because when you delete a field in the node view or graph, it produces '' as the result
  // when the user intended to not have any value in there
  // also emptystring as a field never makes sense for this app
  if (value === '') {
    var valueToSet = undefined;
  }
  else {
    var metadata = mgm.Node.metadataByVariableName[name];
    var type = mgm.Graph.jsTypeByFieldType[metadata.fieldType];
    valueToSet = type(value);
  }
  this.attributes[name] = valueToSet;
  !silent && this.trigger('changed', this);
  name === 'name' && this.updateSVGAppearance();
};

mgm.Node.prototype.update = function(attributes, silent) {
  var somethingChanged = false;
  for (var key in attributes) {
    if (key !== 'id' && attributes[key] !== undefined) {
      this.set(key, attributes[key], true);
      somethingChanged = true;
    }
  }
  !silent && somethingChanged && this.trigger('changed', this);
};

mgm.Node.prototype.save = function() {
  // this.trigger('save', this);
  // maybe do the put request right here
};
