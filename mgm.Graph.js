mgm.Graph = function(data) {
  MicroEvent.mixin(this);
  this.init(data);
  this.addEvents();
  this.changeMode('selectNodes');
  this.render();
};

mgm.Graph.nodeRadius = 12;
mgm.Graph.beaconRadius = 55;
mgm.Graph.beaconXRadius = 35;

mgm.Graph.jsTypeByFieldType = {
  // fieldTypes are my own names for types used in the spreadsheet
  'integer': Number,
  'float': Number,
  'string': String,
  'choice': String,
  'boolean': Boolean
};


mgm.Graph.prototype.init = function(data) {
  this.mode = null; // can be select nodes, links, add nodes/beacons, zoom, etc
  this.setupLayout(data);
  this.links = data.links;
  this.linksActivated = true;
  this.proximityLinks = [];
  this.nodes = data.nodes;
  this.beacons = data.beacons;
  this.$links = this.svg.append('svg:g').selectAll('path');
  this.$nodes = this.svg.append('svg:g').selectAll('.node');
  this.$beacons = this.svg.append('svg:g').selectAll('.beacon');
  this.lasso = {};
  this.selectedLinks = [];
  this.selectedBeacons = [];
  this.lassoedNodes = [];
  this.lastMouseDownCoordinates = null;
  this.mousedown_link = null;
  this.mousedown_node = null;
  this.mousedown_beacon = null;
  this.lastKeyDown = -1;
};

mgm.Graph.prototype.lassoContains = function(thing) {
  if (thing instanceof mgm.Link) {
    return this.lassoContains(thing.source) || this.lassoContains(thing.target);
  }
  else { // it's a node or beacon
    //the greater the radius denominator, the more sensitive the selection
    //8 means you can select 1/4th of the node and still get it
    var increment = mgm.Graph.nodeRadius / 8;
    return (
      thing.x - increment >= this.lasso.x &&
      thing.x + increment <= this.lasso.x + this.lasso.width &&
      thing.y - increment >= this.lasso.y &&
      thing.y + increment <= this.lasso.y + this.lasso.height
    );
  }
};

mgm.Graph.prototype.updateLassoedObjects = function() {
  /* Updates selection data based on what's inside the lasso (can be links or nodes) */
  var self = this;
  if (this.toSelect === 'nodes') {
    this.removeLassoedNodes();
    this.$nodes.each(function(node) {
      if (self.lassoContains(node)) {
        self.lassoedNodes.push(node);
        d3.select(this).select('circle').classed('lassoed', true);
      }
    });
  }
  else {
    this.removeSelectedLinks();
    self.$links.each(function(link){
      if (self.lassoContains(link)) {
        self.selectedLinks.indexOf(link) === -1 && self.selectedLinks.push(link);
        d3.select(this).classed('selected', true);
      }
    });
  }
};

mgm.Graph.prototype.removeLinksAttachedTo = function(node) {
  var removeOptions = {updateGraph: true, updateGrid: true};
  for (var i = this.links.length-1; i >= 0; i--) {
    var link = this.links[i];
    (link.source === node || link.target === node) && this.links.remove(link, removeOptions);
  }
};

mgm.Graph.prototype.deleteLassoedNodes = function() {
  var node;
  this.trigger('beginGridTransaction', 'nodes');
  this.trigger('beginGridTransaction', 'links');
  for (var i = this.lassoedNodes.length-1; i >= 0; i--) {
    node = this.lassoedNodes.splice(i, 1)[0];
    this.removeLinksAttachedTo(node);
    this.nodes.remove(node);
  }
  node && this.trigger('changedLassoedNodes');
  this.trigger('endGridTransaction', 'nodes');
  this.trigger('endGridTransaction', 'links');
};

mgm.Graph.prototype.deleteSelectedLinks = function() {
  var link;
  this.trigger('beginGridTransaction', 'links');
  var removeOptions = {updateGrid: true, updateGraph: true};
  for (var i = this.selectedLinks.length-1; i >= 0; i--) {
    link = this.selectedLinks.splice(i, 1)[0];
    this.links.remove(link, removeOptions);
  }
  this.trigger('endGridTransaction', 'links');
  this.removeSelectedLinks();
};

mgm.Graph.prototype.deleteSelectedBeacons = function() {
  var beacon;
  for (var i = this.selectedBeacons.length-1; i >= 0; i--) {
    beacon = this.selectedBeacons.splice(i, 1)[0];
    this.beacons.remove(beacon, true);
  }
  if (beacon) {
    this.beacons.trigger('removed');
    this.trigger('changedSelectedBeacons');
  }
};

mgm.Graph.prototype.addKeyHandlers = function() {
  d3.select(document)
    .on('keydown', this.getKeyDownHandler())
    .on('keyup',   this.getKeyUpHandler());
};

mgm.Graph.prototype.removeKeyHandlers = function() {
  d3.select(document)
    .on('keydown', null)
    .on('keyup',   null);
};

mgm.Graph.prototype.setupKeyboard = function() {
  // if any handlers were in place (say from the gridview), remove them
  this.removeKeyHandlers();
  // then start intercepting DELETE and SPACEBAR keys
  this.addKeyHandlers();

  // when the user is typing into an input field, don't intercept keystrokes
  $(document).on('focus', 'input.field', this.removeKeyHandlers);
  // when a user moves out of input field, start intercepting keystrokes again
  // unless there is another focused input field
  $(document).on('blur',  'input.field', this.possiblyRewireKeyBoard.bind(this));
};

mgm.Graph.prototype.addEvents = function() {
  this.setupKeyboard();

  this.svg
    .on('mousemove', this.getMouseMoveHandler())
    .on('mousedown', this.getLassoCreator())
    .on('mouseup',   this.getMouseUpHandler());

  this.force
    .on('start',     this.fastForward.bind(this, 0.01, 300))
    .on('tick',      this.tick.bind(this));

  var render = this.render.bind(this);
  this.nodes.bind('added',      render);
  this.nodes.bind('removed',    render);
  this.nodes.bind('changed',    render);
  this.beacons.bind('added',    render);
  this.beacons.bind('removed',  render);
  this.beacons.bind('changed',  render);
  var renderLinks = this.renderLinks.bind(this);
  this.links.bind('added',      renderLinks);
  this.links.bind('addToGraph', renderLinks);
  this.links.bind('removed',    renderLinks);
  this.links.bind('changed',    renderLinks);

  this.bind('changedLassoedNodes',    this.possiblyDisplayIntertwineButton.bind(this));

  $(document).on('click', 'button.mode',        this.handleModeButtonClick.bind(this));
  $(document).on('click', 'button#toggleLinks',      this.toggleLinks.bind(this));
  $(document).on('click', 'button#intertwineButton', this.handleIntertwineClick.bind(this));

  this.bind('changedLassoedNodes', this.updateProximityLinks.bind(this));
};

mgm.Graph.prototype.updateProximityLinks = function() {
  /* Updates which links are attached to currently selected nodes */
  //don't worry about this if we're displaying all links anyway
  if (this.linksActivated) return;
  this.proximityLinks = [];
  for (var i = 0; i < this.links.length; i++) {
    var link = this.links[i];
    if (
      this.lassoedNodes.indexOf(link.source) >= 0 ||
      this.lassoedNodes.indexOf(link.target) >= 0
      ) {
      this.proximityLinks.push(link);
    }
  }
  this.renderLinks();
};

mgm.Graph.prototype.toggleLinks = function() {
  $('button#toggleLinks').toggleClass('activated');
  this.linksActivated = !this.linksActivated;
  this.renderLinks();
};

mgm.Graph.prototype.resetMouseVars = function() {
  this.mousedown_node = null;
  this.mousedown_link = null;
  this.mousedown_beacon = null;
};

mgm.Graph.prototype.tick = function(e) {
  var self = this;
  this.$nodes
    .each(self.moveTowardsBeacons(self.nodes, self.beacons, 10*e.alpha*e.alpha))
    .each(self.resolveCollisions(0.5));
  this.keepNodesInBounds();
};

mgm.Graph.prototype.createOrUpdateLink = function(source, target, options) {
  if (options.updateGraph) {
    // if there's already a link between source&target, make it go both ways
    var link = this.links.filter(function(l) {
      return (
        (l.source === source && l.target === target) ||
        (l.source === target && l.target === source)
      );
    })[0];
    if (link) {
      link.right = link.left = true;
      this.links.trigger('changed');
      this.links.trigger('updateGrid');
      return;
    }
  }
  this.links.create({
    sourceNodeID: source.id,
    targetNodeID: target.id,
    left: options.updateGrid ? true : false,
    right: true
  }, options);
};
