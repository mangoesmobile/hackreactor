mgm.Grid = function(data) {
  /* Contains and manages node and edge grids */
  this.nodes = data.nodes;
  this.links = data.links;
  this.nodeGridView = new mgm.NodeGridView(this.nodes);
  this.linkGridView = new mgm.LinkGridView(this.links, this.nodes);

  this.addEvents();

  this.view = null;
  this.activeGridView = null;
  this.setView('node');
};

mgm.Grid.prototype.setView = function(newView) {
  if (this.view === newView) return;
  if (newView === 'node') {
    $('#linkGridContainer').hide();
    this.activeGridView = this.nodeGridView;
    $('#nodeGridContainer').show();
  }
  else if (newView === 'link') {
    $('#nodeGridContainer').hide();
    this.activeGridView = this.linkGridView;
    $('#linkGridContainer').show();
  }
  this.view = newView;
};

mgm.Grid.prototype.addEvents = function() {
  $(document).on('click', 'button#switchGrid',  this.handleSwitchGridClick.bind(this));
  // on entering edit mode, remove keyhandlers
  // on exit edit mode, make DELETE clear the active cell in the active gridview

  this.nodeGridView.grid.onBeforeEditCell.subscribe(this.removeKeyHandlers);
  this.nodeGridView.grid.onBeforeCellEditorDestroy.subscribe(this.addKeyHandlers.bind(this));
  this.linkGridView.grid.onBeforeEditCell.subscribe(this.removeKeyHandlers);
  this.linkGridView.grid.onBeforeCellEditorDestroy.subscribe(this.addKeyHandlers.bind(this));
};

mgm.Grid.prototype.handleSwitchGridClick = function() {
  this.setView(this.view === 'node' ? 'link' : 'node');
};

mgm.Grid.prototype.beginTransaction = function(gridType) {
  var grid = gridType === 'links' ? this.linkGridView : this.nodeGridView;
  grid.dataView.beginUpdate();
};

mgm.Grid.prototype.endTransaction = function(gridType) {
  var grid = gridType === 'links' ? this.linkGridView : this.nodeGridView;
  grid.dataView.endUpdate();
};

mgm.Grid.prototype.removeKeyHandlers = function() {
  d3.select(document)
    .on('keydown', null)
    .on('keyup', null);
};

mgm.Grid.prototype.handleDelete = function() {
  var cell = this.activeGridView.grid.getActiveCell();
  var item = this.activeGridView.dataView.getItem(cell.row);
  var column = this.activeGridView.columns[cell.cell];
  if (this.view === 'node') {
    // this works because there is no special handling of node attributes
    this.activeGridView.models.get(item.id).set(column.field, '');
  }
  else if (this.view === 'link') {
    // This is a really hacky because changing link attributes via the grid
    // has serious implications (see mgm.LinkGridView and use of FakeLinks for evidence)
    // so, we have to trick SlickGrid into thinking someone is physically editing the cell
    // so SlickGrid will go through its normal chain of event handlers.
    // Otherwise, the FakeLinks procedure does not work and this blows up.
    // Yeah I know this is terrible. Sorry.
    var grid = this.activeGridView.grid;
    grid.editActiveCell();
    editor = grid.getCellEditor();
    // setValue is something I monkey-patched into slick.editors.js (based on existing slickJS code)
    // it changes the $input of the grid editor
    editor.setValue(undefined);
    grid.navigateNext() || grid.navigatePrev();
  }
};

mgm.Grid.prototype.getKeyDownHandler = function() {
  var self = this;
  var keyCodeHandlers = {
    8: this.handleDelete, //DELETE
    46: this.handleDelete //DELETE
  };
  return function() {
    if (!(d3.event.keyCode in keyCodeHandlers)) return;
    d3.event.preventDefault();
    var fn = keyCodeHandlers[d3.event.keyCode];
    fn.call(self);
  };
};

mgm.Grid.prototype.addKeyHandlers = function() {
  d3.select(document)
    .on('keydown', this.getKeyDownHandler())
    .on('keyup', null);
};

mgm.Grid.prototype.setupKeyboard = function() {
  this.removeKeyHandlers();
  this.addKeyHandlers();
};
