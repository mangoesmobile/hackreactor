var http = require('http');
var fs = require('fs');
var path = require('path');


var headers = {};


var getContentType = function(ext) {
    var ct;
    switch (ext) {
    case '.html':
      ct = 'text/html';
      break;
    case '.css':
      ct = 'text/css';
      break;
    case '.js':
      ct = 'text/javascript';
      break;
    default:
      ct = 'text/plain';
      break;
    }
    return ct;
};

var handleRequest = function(req, res) {
  if (req.url === '/') {
    var indexFile = __dirname + '/index.html';
    fs.readFile(indexFile, function(err, html) {
      if (err) {
        console.log(err);
        res.writeHead(503, headers);
        res.end();
      }
      else {
        headers['Content-Type'] = 'text/html';
        headers['Content-Length'] = html.length;
        res.writeHead(200, headers);
        res.write(html);
        res.end();
      }
    });
  }
  else {
    var filePath = __dirname + '/' + req.url;
    fs.readFile(filePath, function(err, data) {
      if (err) {
        res.writeHead(404, headers);
        res.end();
      }
      else {
        headers['Content-Type'] = getContentType(path.extname(filePath));
        headers['Content-Length'] = data.length;
        res.writeHead(200, headers);
        res.end(data);
      }
    });
  }
};

http.createServer(handleRequest).listen(80);
