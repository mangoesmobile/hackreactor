mgm.EditView = function(graph) {
  this.graph = graph;
  this.$el = $('#info');
  this.render();
};

mgm.EditView.prototype.render = function() {
  this.$el.empty();
  this.$el.append('<p>' + this.graph.lassoedNodes.length + ' nodes selected!</p>');
  this.$el.append('<p>' + this.graph.selectedBeacons.length + ' beacons selected!</p>');
  var self = this;
  this.graph.lassoedNodes.forEach(function(node){
    var nodeView = new mgm.NodeView(node);
    self.$el.append(nodeView.$el);
  });
  this.graph.selectedBeacons.forEach(function(beacon){
    var beaconView = new mgm.BeaconView(beacon);
    self.$el.append(beaconView.$el);
  });
};
