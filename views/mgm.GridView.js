mgm.GridView = function(models, Model) {
  /* Creates a spreadsheet view of the data using:
  https://github.com/mleibman/SlickGrid/wiki
  This is an abstract class that is used differently for nodes vs links
  */
  MicroEvent.mixin(this);

  this.models = models;
  this.Model = Model;
  var className = Model === mgm.Node ? 'Node' : 'Link';

  this.dataView = new Slick.Data.DataView();
  this.dataView.setItems(this.getRows(models));
  this.columns = this.getColumns();

  var options = {
    enableCellNavigation: true,
    enableColumnReorder: false,
    editable: true,
    autoEdit: false,
    asyncEditorLoading: false,
    enableAddRow: true
  };
  this.grid = new Slick.Grid(
    className === 'Node' ? '#grid' : '#linkGrid',
    this.dataView,
    this.columns,
    options
  );
  this.grid.autosizeColumns();
  this.grid.setSelectionModel(new Slick.CellSelectionModel());
  this.copyManager = new Slick.CellExternalCopyManager();
  this.grid.registerPlugin(this.copyManager);

  var wizardFactory = mgm[className + 'WizardView'];
  this.wizardView = new wizardFactory(Model);
  this.addEvents();
};

mgm.GridView.editorByFieldType = {
  'string': Slick.Editors.Text,
  'integer': Slick.Editors.Integer,
  // boolean for us is actually a ternary boolean
  'boolean': Slick.Editors.Choice.bind(this, [undefined, true, false]),
  'float': Slick.Editors.Float,
  'choice': Slick.Editors.Choice
};


mgm.GridView.prototype.addEvents = function() {
  var self = this;
  this.dataView.onRowCountChanged.subscribe(function(e, args) {
    self.grid.updateRowCount();
    self.grid.render();
  });
  this.dataView.onRowsChanged.subscribe(function(e, args) {
    self.grid.invalidateRows(args.rows);
    self.grid.render();
  });

  this.models.bind('changed', this.updateDataView.bind(this, 'change'));
  this.models.bind('removed', this.updateDataView.bind(this, 'remove'));
  this.models.bind('added',   this.updateDataView.bind(this, 'add'));

  this.grid.onCellChange.subscribe( this.handleCellChange.bind(this));
  this.grid.onAddNewRow.subscribe(  this.handleNewRow.bind(this));

  this.copyManager.onBeforePasteCells.subscribe( this.handleExternalBeforePaste.bind(this));
  this.copyManager.onPasteCells.subscribe(       this.handleExternalPaste.bind(this));

  this.wizardView.bind('addAttribute',    this.handleAddAttribute.bind(this));
  this.wizardView.bind('removeAttribute', this.handleRemoveAttribute.bind(this));
  this.wizardView.bind('renameAttribute', this.handleRenameAttribute.bind(this));
};

mgm.GridView.prototype.updateDataView = function(mode, model){
  if (mode === 'remove') {
    this.dataView.deleteItem(model.id);
  }
  else if (mode === 'add') {
    model && this.dataView.addItem(model.attributes);
  }
  else {
    // TODO: only invalidate the row of the given model's id
    this.grid.invalidate();
  }
};

mgm.GridView.prototype.getItemFromPasteData = function(rowArrayToPaste, startColumn) {
  /* The external paste manager gives you data in the form of an array.
  This function converts those arrays into objects based on
  the columns that coincide with the array indices
  */
  var item = {};
  var columnIndex = startColumn;
  for (var i = 0; i < rowArrayToPaste.length; i++) {
    var variableName = this.columns[columnIndex].field;
    item[variableName] = rowArrayToPaste[i];
    columnIndex++;
  }
  return item;
};

mgm.GridView.prototype.getColumn = function(variableName) {
  var column = {
    id: variableName + '_id',
    name: variableName.toUpperCase(),
    field: variableName
  };
  var metadata = this.Model.metadataByVariableName[variableName];
  if (!metadata.readOnly) {
    //only make the column editable if it's not read-only
    var editor = mgm.GridView.editorByFieldType[metadata.fieldType];
    if (!metadata.choices) {
      column.editor = editor;
    }
    else {
      column.editor = editor.bind(this, metadata.choices);
    }
  }
  return column;
};

mgm.GridView.prototype.getColumns = function() {
  var columns = [];
  for (var variableName in this.Model.metadataByVariableName) {
    columns.push(this.getColumn(variableName));
  }
  return columns;
};

mgm.GridView.prototype.getRows = function(models) {
  var rows = [];
  for (var i = 0; i < models.length; i++) {
    // by default, rows come from model .attributes property
    // which is part of the reason why I made Link's .attributes refer to itself
    rows.push(models[i].attributes);
  }
  return rows;
};

mgm.GridView.prototype.deleteActiveCellContents = function() {
  var cell = this.getActiveCell();
};
