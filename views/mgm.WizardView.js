mgm.WizardView = function(Model) {
  MicroEvent.mixin(this);
  this.Model = Model;
  this.permanentColumns = {id: 1};
  this.divCssSelector = '#' + (Model === mgm.Node ? 'node' : 'link') + 'Wizard';
  this.$el = $(this.divCssSelector);
  this.addEvents();
};

mgm.WizardView.prototype.addingIsValid = function(name) {
  /* Don't allow duplicate names or falsy names */
  return name && !(name in this.Model.metadataByVariableName);
};

mgm.WizardView.prototype.handleAddAttributeClick = function() {
  var name = this.$addColumnNameInput.val();
  var type = this.$addColumnTypeSelect.val();
  var choices = type === 'choice' ? this.$choicesInput.val().split(',') : undefined;
  this.$addColumnNameInput.val('');
  this.$addColumnTypeSelect.val('');
  this.$choicesInput.val('');
  //TODO: display error
  if (this.addingIsValid(name)) this.trigger('addAttribute', name, type, choices);
  else console.log('cannot add attribute ' + name);
};

mgm.WizardView.prototype.handleAddAttributeTypeChange = function() {
  type = this.$addColumnTypeSelect.val();
  if (type === 'choice') {
    this.$pChoices.show();
  }
  else {
    this.$pChoices.hide();
  }
};

mgm.WizardView.prototype.removingIsValid = function(name) {
  /* don't remove columns that either don't exist or are protected by permanent status */
  return (
    name in this.Model.metadataByVariableName &&
    !(name in this.permanentColumns)
  );
};

mgm.WizardView.prototype.handleRemoveAttributeClick = function() {
  var name = this.$removeColumnSelect.val();
  this.$removeColumnSelect.val('');
  // TODO: display error
  if (this.removingIsValid(name)) this.trigger('removeAttribute', name);
  else console.log('cannot remove attribute ' + name);
};

mgm.WizardView.prototype.renamingIsValid = function(oldName, newName) {
  /* Don't rename something that doesn't exist, and
  don't overwrite existing column's name, and
  don't change columns that are protected by permanent status
  */
  return (
    (oldName in this.Model.metadataByVariableName) &&
    !(newName in this.Model.metadataByVariableName) &&
    !(oldName in this.permanentColumns)
  );
};

mgm.WizardView.prototype.handleRenameAttributeClick = function() {
  var name = this.$renameColumnSelect.val();
  var newName = this.$renameColumnInput.val();
  this.$renameColumnSelect.val('');
  this.$renameColumnInput.val('');
  // TODO: display error
  if (this.renamingIsValid(name, newName)) this.trigger('renameAttribute', name, newName);
  else console.log('cannot rename attribute ' + name + ' to ' + newName);
};

mgm.WizardView.prototype.addEvents = function() {
  $(document).on('click', this.divCssSelector + ' button.addAttribute',    this.handleAddAttributeClick.bind(this));
  $(document).on('click', this.divCssSelector + ' button.removeAttribute', this.handleRemoveAttributeClick.bind(this));
  $(document).on('click', this.divCssSelector + ' button.renameAttribute', this.handleRenameAttributeClick.bind(this));
  $(document).on('change', 'select.wizard#addType', this.handleAddAttributeTypeChange.bind(this));
};

mgm.WizardView.prototype.getAddColumnForm = function() {
  // TODO refactor to use template
  this.$addColumnNameInput = $('<input class="wizard" type="text"/>');
  this.$addColumnTypeSelect = $('<select id="addType" class="wizard"/>');
  for (var fieldType in mgm.Graph.jsTypeByFieldType){
    var $choice = $('<option/>').text(fieldType);
    this.$addColumnTypeSelect.append($choice);
  }
  var $pName = $('<span>Column to Add: </span>').append(this.$addColumnNameInput)
  var $pType = $('<span>Type: </span>').append(this.$addColumnTypeSelect)
  this.$choicesInput = $('<input type="text"/>');
  this.$pChoices = $('<span>Choices (must be a comma separated list): </span>')
    .append(this.$choicesInput);
  this.$pChoices.hide();
  var $addColumnButton = $('<button class="addAttribute">Add Column</button>');
  return $('<div class="wizard form"/>')
    .append($pName)
    .append($pType)
    .append(this.$pChoices)
    .append($addColumnButton);
};

mgm.WizardView.prototype.getRemoveColumnForm = function() {
  // TODO refactor to use template
  this.$removeColumnSelect = $('<select class="wizard"/>');
  for (var variableName in this.Model.metadataByVariableName){
    if (variableName in this.permanentColumns) continue;
    var $choice = $('<option/>').text(variableName);
    this.$removeColumnSelect.append($choice);
  }
  var $pRemoveSelect = $('<span>Column to Remove: </span>').append(this.$removeColumnSelect)
  var $removeColumnButton = $('<button class="removeAttribute">Remove Column</button>');
  return $('<div class="wizard form"/>')
    .append($pRemoveSelect)
    .append($removeColumnButton);
};

mgm.WizardView.prototype.getRenameColumnForm = function() {
  // TODO refactor to use template
  this.$renameColumnSelect = $('<select class="wizard"/>');
  for (var variableName in this.Model.metadataByVariableName){
    if (variableName in this.permanentColumns) continue;
    var $choice = $('<option/>').text(variableName);
    this.$renameColumnSelect.append($choice);
  }
  var $pRenameSelect = $('<span>Column to Rename: </span>').append(this.$renameColumnSelect);
  this.$renameColumnInput = $('<input class="wizard" type="text"/>');
  var $pRenameInput = $('<span>New name: </span>').append(this.$renameColumnInput);
  var $renameColumnButton = $('<button class="renameAttribute">Rename Column</button>');
  return $('<div class="wizard form"/>')
    .append($pRenameSelect)
    .append($pRenameInput)
    .append($renameColumnButton);
};

mgm.WizardView.prototype.render = function() {
  // TODO: give choices for special metadata like readonly and choices
  this.$el.empty();
  this.$addColumnForm = this.getAddColumnForm()
  this.$el.append(this.$addColumnForm);
  this.$el.append(this.getRemoveColumnForm());
  this.$el.append(this.getRenameColumnForm());
};
