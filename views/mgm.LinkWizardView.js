mgm.LinkWizardView = function() {
  mgm.WizardView.call(this, mgm.Link);
  // make sure these D3-specific link properties are not messed with
  this.permanentColumns = {
    'left': 1, 'right': 1,
    'source': 1, 'target': 1,
    'id': 1,
    'targetNodeID': 1, 'sourceNodeID': 1
  };
  this.render();
};

mgm.LinkWizardView.prototype = Object.create(mgm.WizardView.prototype);
mgm.LinkWizardView.constructor = mgm.LinkWizardView;
