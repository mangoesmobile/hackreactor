mgm.AbstractView = function(model, className) {
  /* Base class for Node and Beacon views,
  which are displayed below the graph for each lassoed node/beacon
  */
  this.model = model;
  this.class = className;
  this.$el = $('<div/>').addClass(this.class);
  this.render();
};

mgm.AbstractView.prototype.getFieldFactory = function(metadata) {
  var fields = {
    'string': this.getTextInputField,
    'boolean': this.getChoicesField,
    'float': this.getTextInputField,
    'integer': this.getTextInputField,
    'choice': this.getChoicesField
  };
  if (metadata.readOnly) return this.getTextReadOnlyField;
  var fieldFactory = fields[metadata.fieldType]
  if (metadata.choices) {
    return fieldFactory.bind(0, metadata.choices);
  }
  else if (metadata.fieldType === 'boolean') {
    return fieldFactory.bind(0, [undefined, true, false]);
  }
  return fieldFactory;
};

mgm.AbstractView.prototype.getTextReadOnlyField = function(value, variableName) {
  return $('<input class="field" type="text" disabled>').val(value);
};

mgm.AbstractView.prototype.getTextInputField = function(value, variableName) {
  var $input = $('<input class="field" type="text">')
  return value !== undefined ? $input.val(value) : $input.attr('placeholder', variableName)
};

mgm.AbstractView.prototype.getChoicesField = function(choices, value, variableName) {
  var $select = $('<select class="field"/>');
  choices.forEach(function(choice){
    var $choice = $('<option class="field"/>').text(choice);
    choice === value && $choice.prop('selected', true);
    $select.append($choice);
  });
  return $select;
};

mgm.AbstractView.prototype.saveField = function(evt) {
  var $field = $(evt.target);
  var variableName = $field.data('variableName');
  var value = $field.val();
  // quick hack to make true/false/null ternary choice fields work
  if (value === 'true') value = true;
  if (value === 'false') value = false;
  this.model.set(variableName, value)
};

mgm.AbstractView.prototype.render = function() {
  /* Create inputs for each of the node's attributes */
  var $title = $('<p/>').text(this.class.toUpperCase());
  this.$el.append($title);
  // assumes that each model will have identical attributes
  for (var variableName in mgm.Node.metadataByVariableName) {
    var $statLine = [$('<span/>').text(variableName + ': ')];
    var value = this.model.attributes[variableName]
    var metadata = mgm.Node.metadataByVariableName[variableName];
    var fieldFactory = this.getFieldFactory(metadata);
    var $field = fieldFactory(value, variableName);
    $field.data('variableName', variableName);
    $field.on('change', this.saveField.bind(this));
    $statLine.push($field);
    var $statdiv = $('<div/>').append($statLine);
    this.$el.append($statdiv);
  }
};
