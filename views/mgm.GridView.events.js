mgm.GridView.prototype.handleCellChange = function(e, args) {
  this.models.get(args.item.id).update(args.item, args.silent);
};

mgm.GridView.prototype.handleExternalPaste = function(e, args) {
  /* After a paste is over, update models because the cellexternalcopymanager plugin
  skips internal type-casting and event systems.
  */
  // ranges is only [0] because the externalcopymanager only supports copy/pasting one range at a time
  for (var i = args.ranges[0].fromRow; i <= args.ranges[0].toRow; i++) {
    this.handleCellChange(null, {item: this.dataView.getItemByIdx(i), silent: true});
  }
  this.dataView.endUpdate();
  this.models.trigger('changed');
};

mgm.GridView.prototype.handleNewRow = function (e, args) {
  this.models.create({attributes: args.item});
};

mgm.GridView.prototype.handleExternalBeforePaste = function(e, args) {
  /* Warning: this is really hacky.
  The 3rd party externalpastemanager plugin does not play nice with dataviews.
  It will not add new rows to a dataview IF they would go beyond the
  currently existing number of rows in the table.
  So by default, it won't expand the length of the table on paste.

  This function gets the pasted items that will be positioned > current end of table
  and manually creates rows for them.
  */
  this.dataView.beginUpdate();
  var copyManager = args.copyManager;
  var startRow = copyManager.activeRow;
  var endRow = startRow + copyManager.clippedRange.length-1;
  var currentRowEnd = this.dataView.getLength()-1;
  for (var i = startRow; i <= endRow; i++) {
    if (i > currentRowEnd) {
      var rowArrayToPaste = copyManager.clippedRange[i - startRow];
      var attributes = this.getItemFromPasteData(rowArrayToPaste, copyManager.activeCell);
      // don't trigger the added event so the graph doesn't update visually on every create
      var model = this.models.create({attributes: attributes}, true);
      this.dataView.addItem(model.attributes);
    }
  }
};

mgm.GridView.prototype.handleAddAttribute = function(name, fieldType, choices) {
  /* Adds the new column name as a field in the node edit views
  and adds a new column to th grid
  */
  var metadata = {fieldType: fieldType};
  if (choices) metadata.choices = choices;
  this.Model.metadataByVariableName[name] = metadata;
  this.columns.push(this.getColumn(name));
  this.grid.setColumns(this.columns);
  this.trigger('addedAttribute');
};

mgm.GridView.prototype.handleRemoveAttribute = function(name) {
  /* Removes a column from the grid, from the edit views, and from each model */
  for (var i = 0; i < this.columns.length; i++) {
    this.columns[i].field === name && this.columns.splice(i, 1);
  }
  this.grid.setColumns(this.columns);
  delete this.Model.metadataByVariableName[name];
  this.models.forEach(function(model){
    delete model.attributes[name];
  });
  this.trigger('removedAttribute');
};

mgm.GridView.prototype.handleRenameAttribute = function(name, newName) {
  /* Renames a column in the grid, the edit views, and in each model */
  mgm.Util.renameProperty(this.Model.metadataByVariableName, name, newName);
  this.models.forEach(function(model){
    mgm.Util.renameProperty(model.attributes, name, newName);
  });
  for (var i = 0; i < this.columns.length; i++) {
    if (this.columns[i].field === name) {
      this.columns[i] = this.getColumn(newName);
    }
  }
  this.grid.setColumns(this.columns);
  this.trigger('renamedAttribute');
};
