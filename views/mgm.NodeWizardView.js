mgm.NodeWizardView = function() {
  mgm.WizardView.call(this, mgm.Node);
  this.render();
};

mgm.NodeWizardView.prototype = Object.create(mgm.WizardView.prototype);
mgm.NodeWizardView.constructor = mgm.NodeWizardView;
