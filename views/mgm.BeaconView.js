mgm.BeaconView = function(beacon) {
  mgm.AbstractView.call(this, beacon, 'beacon');
};

mgm.BeaconView.prototype = Object.create(mgm.AbstractView.prototype);
mgm.BeaconView.constructor = mgm.BeaconView;
