mgm.NodeView = function(node) {
  mgm.AbstractView.call(this, node, 'node');
};

mgm.NodeView.prototype = Object.create(mgm.AbstractView.prototype);
mgm.NodeView.constructor = mgm.NodeView;
