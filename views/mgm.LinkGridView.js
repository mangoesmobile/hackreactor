mgm.LinkGridView = function(links, nodes){
  this.fakeLinks = new mgm.FakeLinks(nodes);
  mgm.GridView.call(this, links, mgm.Link);
};

mgm.LinkGridView.prototype = Object.create(mgm.GridView.prototype);
mgm.LinkGridView.constructor = mgm.LinkGridView;

mgm.LinkGridView.prototype.handleNewRow = function (e, args) {
  var wouldBeOK = mgm.Link.prototype.isValid.call(args.item);
  if (wouldBeOK) {
    //I assume this OK condition only happens when an entire, complete row is pasted in at once
    this.models.create(args.item, {updateGrid: true, updateGraph: false});
  }
  else {
    this.fakeLinks.create(args.item, {updateGrid: true, updateGraph: false});
  }
};

mgm.LinkGridView.prototype.handleCellChange = function(e, args) {
  if (!args.item._fake) {
    var link = this.models.update(args.item.id, args.item);
    if (!link.isValid()) {
      //the change invalidated it, so remove it from actual links and add to toCreate list
      this.makeFake(link);
    }
  }
  else {
    var fakeLink = this.fakeLinks.update(args.item.id, args.item);
    if (fakeLink.isValid()) {
      //the change validated it, so remove it from fake links and add to real links
      this.makeReal(fakeLink);
    }
  }
};

mgm.LinkGridView.prototype.makeFake = function(link) {
  this.models.remove(link, {updateGrid: true, updateGraph: true});
  this.fakeLinks.create(link, {updateGrid: true, updateGraph: false});
};

mgm.LinkGridView.prototype.makeReal = function(fakeLink) {
  this.fakeLinks.remove(fakeLink, {updateGrid: true, updateGraph: false});
  this.models.create(fakeLink, {updateGrid: true, updateGraph: true});
};

mgm.LinkGridView.prototype.addEvents = function() {
  var self = this;
  this.dataView.onRowCountChanged.subscribe(function(e, args) {
    self.grid.updateRowCount();
    self.grid.render();
  });
  this.dataView.onRowsChanged.subscribe(function(e, args) {
    self.grid.invalidateRows(args.rows);
    self.grid.render();
  });

  // special event handlers used to separate grid updates from graph updates
  // the rest of the event handlers below and above this are the same as before
  this.models.bind('updateGrid',     this.updateDataView.bind(this, 'change'));
  this.models.bind('addToGrid',      this.updateDataView.bind(this, 'add'));
  this.models.bind('removeFromGrid', this.updateDataView.bind(this, 'remove'));
  this.fakeLinks.bind('updateGrid',     this.updateDataView.bind(this, 'change'));
  this.fakeLinks.bind('addToGrid',      this.updateDataView.bind(this, 'add'));
  this.fakeLinks.bind('removeFromGrid', this.updateDataView.bind(this, 'remove'));

  this.grid.onCellChange.subscribe( this.handleCellChange.bind(this));
  this.grid.onAddNewRow.subscribe(  this.handleNewRow.bind(this));

  this.copyManager.onBeforePasteCells.subscribe( this.handleExternalBeforePaste.bind(this));
  this.copyManager.onPasteCells.subscribe(       this.handleExternalPaste.bind(this));

  this.wizardView.bind('addAttribute',    this.handleAddAttribute.bind(this));
  this.wizardView.bind('removeAttribute', this.handleRemoveAttribute.bind(this));
  this.wizardView.bind('renameAttribute', this.handleRenameAttribute.bind(this));
};

mgm.LinkGridView.prototype.getColumn = function(variableName) {
  var column = mgm.GridView.prototype.getColumn.call(this, variableName);
  if (variableName === 'left') {
    column.name = 'TowardsSource';
  }
  if (variableName === 'right') {
    column.name = 'TowardsTarget';
  }
  return column;
};
