mgm.NodeGridView = function(nodes) {
  mgm.GridView.call(this, nodes, mgm.Node);
};

mgm.NodeGridView.prototype = Object.create(mgm.GridView.prototype);
mgm.NodeGridView.constructor = mgm.NodeGridView;
